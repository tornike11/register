package com.example.register

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.CheckBox
import android.widget.EditText
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    private lateinit var regButton: Button
    private lateinit var editPassword: EditText
    private lateinit var editPassword2: EditText
    private lateinit var editMail:EditText
    private lateinit var acceptedCheck: CheckBox
    private lateinit var editName: EditText
    private lateinit var editSurname: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        regButton = findViewById(R.id.regButton)
        editMail = findViewById(R.id.editMail)
        acceptedCheck = findViewById(R.id.acceptedCheck)
        editPassword = findViewById(R.id.editPassword)
        editPassword2 = findViewById(R.id.editPassword2)
        editName = findViewById(R.id.editName)
        editSurname = findViewById(R.id.editSurname)



        regButton.setOnClickListener {
        if(editName.text.isEmpty()){
            Toast.makeText(this, "სახელის ველი ცარიელია", Toast.LENGTH_LONG).show()
            }
        else if(editSurname.text.isEmpty()){
            Toast.makeText(this, "გვარის ველი ცარიელია", Toast.LENGTH_LONG).show()
        }
        else if(editPassword.text.isEmpty()){
            Toast.makeText(this, "პაროლის ველი ცარიელია", Toast.LENGTH_LONG).show()
        }
        else if(editPassword2.text.isEmpty()) {
            Toast.makeText(this, "მეორედ ჩასაწერი პაროლის ველი ცარიელია", Toast.LENGTH_LONG).show()
        }
        else if(editMail.text.isEmpty()){
            Toast.makeText(this, "Mail-ის ჩასაწერი ველი ცარიელია", Toast.LENGTH_LONG).show()
        }
        else if(editMail.text.contains("@") == false){
            Toast.makeText(this, "შეამოწმე მეილი", Toast.LENGTH_LONG).show()
        }
        else if(editPassword.text.toString() != editPassword2.text.toString()) {
            Toast.makeText(this, "შეყვანილი პაროლები ერთმანეთს არ ემთხვევა", Toast.LENGTH_LONG).show()
        }
        else if(!acceptedCheck.isChecked){
            Toast.makeText(this, "დაეთანხმეთ წესებს CheckBox ის მონიშვნით", Toast.LENGTH_LONG).show()
        }else{
            Toast.makeText(this, "თქვენ წარმატებით დარეგისტრირდით", Toast.LENGTH_LONG).show()
        }

        }
    }
}